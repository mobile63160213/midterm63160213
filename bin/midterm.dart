import 'package:midterm/midterm.dart' as midterm;
import 'dart:io';

void main(List<String> arguments) {
  print("Input your mathematical experssion");
  var string = stdin.readLineSync()!;
  var infix = tokenizing(string);
  print('Your infix : $infix');
  var postfix = checkPostfix(infix);
  print('Your postfix : $postfix');
  var result = evaluatePostfix(postfix);
  print('YOur result after evaluate postfix : $result');
}

List tokenizing(String string){
  List result = [];
  for (var i = 0; i < string.length; i++){
    //loop for remove space
    if (string[i] == ' '){
      //When to index equla space
      result.add(string[i]);
      result.remove(string[i]);
    } else {
      result.add(string[i]);
    }
  }
  result = tokenizingNegative(result);

  return result;
}

List tokenizingNegative(List tokens){
  for (var i = 0; i < tokens.length -1 ; i++){
    for (var j = i + 1; j < tokens.length; j++) {
      if (tokens[i] == '-' && tokens[j] == '('){
        break;
      } else if (tokens[i] == '-'){
        var temp = tokens[j];
        tokens[i] = '-$temp';
        tokens.remove(tokens[j]);
      }
    }
  }
  return tokens;
}


List checkPostfix(List infix) {
  List operators = [];
  List postfix = [];
  infix.forEach((i) {
    if(isInteger(i)) {
      postfix.add(i);
    } else if (isInteger(i) == false && i != '(' && i != ')') {
      while(operators.isNotEmpty && operators.last != '(' && precedence(i) < precedence(operators.last)) {
        postfix.add(operators.removeLast());
      }
      operators.add(i);
    } else if (i=='('){
      operators.add(i);
    } else if (i==')'){
      while(operators.last != '(') {
        postfix.add(operators.removeLast());
      }
      operators.remove('(');
    }
  });
  while(operators.isNotEmpty) {
    postfix.add(operators.removeLast());
  }
  return postfix;
}

int precedence(String operators) {
  if (operators == "(" || operators == ")") {
    return 4;
  }
  if (operators == "^") {
    return 3;
  }
  if (operators == "*" || operators == "/") {
    return 2;
  }
  if (operators == "%") {
    return 3;
  }
  if (operators == "+" || operators == "-") {
    return 1;
  }
  return 0;
}

bool isInteger(String string) {
  if (string == null) {
    return false;
  }
  return double.tryParse(string) != null;
}

double evaluatePostfix(List postfix) {
  List values = [];
  double left, right;

  postfix.forEach((i) {
    if (isInteger(i)) {
      double temp = double.parse(i);
      values.add(temp);
    } else {
      right = values.removeLast();
      left = values.removeLast();
      var result = calculator(left, right, i);
      values.add(result);
    }
  });

  return values.first;
}

double calculator(double left, double right, String operator) {
  double result = 0;
  switch (operator) {
    case "+":
      {
        result = left + right;
      }
      break;
    case "-":
      {
        result = left - right;
      }
      break;
    case "*":
      {
        result = left * right;
      }
      break;
    case "/":
      {
        result = left / right;
      }
      break;
    case "%":
      {
        result = left % right;
      }
      break;
    case "^":
      {
        result = exponent(left, right);
      }
      break;
    default:
      {}
      break;
  }

  return result;
}

double exponent(double x, double y) {
  double left = x.toDouble();
  double right = y.toDouble();
  double expo = left;
  double result = 1;
  for (int i = 0; i < right; i++) {
    result = result * expo;
  }
  //print("Result : $result");
  return result;
}

